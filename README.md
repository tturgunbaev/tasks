# Tasks

Tasks is a local project for learning with git, GitLab, venv.

## Description

This repositry includes .gitignore, README.md, and *.py files.

## Installation

```shell script
pip install -r requirements.txt
```
in main.py used this libray
```python
import requests
```

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

## License
[MIT](https://choosealicense.com/licenses/mit/)

