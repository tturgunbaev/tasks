#Каждый из N школьников некоторой школы знает Mᵢ языков.
#Определите, какие языки знают все школьники и языки, которые знает хотя бы один из школьников.

print("Входные данные: ")
number_of_students = int(input())
languages = {}

for i in range(1, number_of_students + 1):
    number_of_languages = int(input())
    if number_of_languages > 500:
        print("Количество языков превышает 5000")
        break
    for j in range(number_of_languages):
        language = input().lower()
        if len(language) > 1000:
            print("Название языка более 1000 символов")
            break
        if language in languages:
            languages[language] += 1
        else:
            languages[language] = 1


well_known_languages = [key for key, value in languages.items() if value == number_of_students]

print("")
print("Выходные данные: ")
print(len(well_known_languages))
for i in well_known_languages:
    print(i.capitalize())
print(len(languages))
for i in languages:
    print(i.capitalize())



