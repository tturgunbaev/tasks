"""На Новом проспекте для разгрузки было решено пустить два новых автобусных маршрута на разных участках проспекта.
Известны конечные остановки каждого из автобусов.
Определите количество остановок, на которых можно пересесть с одного автобуса на другой."""

lines = []

for i in range(4):
    while True:
        number = int(input())
        if number <= 100:
            break
        print(f"Number {number} is great than 100, enter again: ")
    lines.append(number)

def busstops(first, last):
    if first == last:
        return [first]
    elif first < last:
        return range(first, last + 1)
    else:
        return range(last, first + 1)
first_lines = busstops(lines[0], lines[1])
second_line = busstops(lines[2], lines[3])

common_busstops = [x for x in first_lines if x in second_line]

print(f"Common number of busstops is {len(common_busstops)}")