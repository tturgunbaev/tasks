import requests

def get_html(url):
    response = requests.get(url)
    html = response.text

    if response.status_code == 200:
        print(f"The status code is good - {response.status_code}")
        file_name = "result.txt"
        with open(file_name, "w") as fl:
            fl.write(html)
            print(f"We get url {url}, and saved url text to file {file_name}")
    else:
        print(f"Something goes wrong, we get status code {response.status_code}. Please check url {url}")

if __name__ == "__main__":
    url = "http://kino.kg"
    get_html(url)