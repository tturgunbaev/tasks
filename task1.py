"""Август и Беатриса продолжают играть в игру, но Август начал жульничать. На каждый из вопросов Беатрисы он выбирает
такой вариант ответа YES или NO, чтобы множество возможных задуманных чисел оставалось как можно больше.
Например, если Август задумал число от 1 до 5, а Беатриса спросила про числа 1 и 2, то Август ответит NO,
а если Беатриса спросит про 1, 2, 3, то Август ответит YES.
Если же Бетриса в своем вопросе перечисляет ровно половину из задуманных чисел, то Август из вредности всегда отвечает NO.
Наконец, Август при ответе учитывает все предыдущие вопросы Беатрисы и свои ответы на них,
то есть множество возможных задуманных чисел уменьшается."""

def text_to_list(text):
    result = [int(x) for x in text.strip().split()]
    return result

def list_to_text(some_list):
    result = [str(x) for x in some_list]
    return " ".join(result)

max_value = int(input())
initial_values = [x for x in range(1, max_value + 1)]

while True:
    answers = input()
    if answers.lower() == 'help':
        print(list_to_text(initial_values))
        break
    if len(text_to_list(answers)) <= len(initial_values) / 2:
        print("NO")
        initial_values = [x for x in initial_values if x not in text_to_list(answers)]
    else:
        print("YES")
        initial_values = [x for x in initial_values if x in text_to_list(answers)]